# Free TON Solidity-lite

<center>

![logo](images/icon.png)

</center>


This extension adds Free TON Solidity language support over Ethereum solidity. This extension is a submission for contest \#36 Syntax highlighting for Solidify files written for Free TON - VSCode.

---

## Features

The extension is following [Free TON Solidity API](https://github.com/tonlabs/TON-Solidity-Compiler/blob/master/API.md) to support new language features.

### Syntax highlighting
○ keywords  
○ variables  
○ literals  
○ comments  
○ other things from the language specification and Free TON additions

**The extension also highlights:**
* TON units
* new keywords like `repeat`
* new modifiers like `responsive`, `functionID`, 'inline', 'externalMsg', 'internalMsg'
* new types, like `fixed`, `ufixed128x20`, etc
* new classes `ExtraCurrencyCollection`, `TvmBuilder`, `TvmCell`, `TvmSlice`
* global Free TON namespaces:
    * `tvm`
    * `rnd`
    * `math`
* global functions: `bitSize`, `format`, `gasToValue`, `stoi`, `tvmlog`, `uBitSize`, `valueToGas`
* special contract methods `afterSignatureCheck`, `fallback`, `onBounce`, `onCodeUpgrade`, `onTickTock`, `receive`
* proper escaped strings support (missed in original extension)
* and other

![syntax](images/syntax.png)


### Code completion
○ keywords  
○ variable names  
○ classes names  
○ method names  
○ <strike>Interfaces</strike>

**It also supports the following:**
* Every method in global namespaces:
    * `tvm` ![tvm](images/tvm.gif)
    * `rnd` ![rnd](images/rnd.gif)
    * `math` ![math](images/math.gif)
* Added and Modified methods and properties in global variables `block`, `msg`, `tx`
* `address` static constructors
* Every new global function ![glob](images/glob.gif)
* New types incluging `fixed`, `ufixed`
* Free TON `pragma`s  ![pragma](images/pragma.gif)
* and more


## Requirements
Your Visual Studio Code should be up do date.

## Installation
You can install the extension by downloading [free-ton-solidity-lite-1.0.5.vsix](free-ton-solidity-lite-1.0.5.vsix) file and dragging it to Extensions tab of Visual Studio Code.

![install](images/install.gif)


## Known issues
I had not enough time to fix all issues of the extension, so there are known issues, which will be fixed later.

1. There were problems with freeton-solc integration and it's disabled at the moment. Error underlining dows not work because of this.
2. Interfaces code completion is not ready at the moment.
3. Not in marketplace.

## Contact

Contact: @pashaborg (Telegram)

## Credits

Many thanks to Zixuan Tan and Juanfranblanco for the vscode extension https://github.com/tan-zixuan/vscode-solidity-lite.
