'use strict';
import * as solparse from 'solparse-exp-jb';
import {ContractCollection} from './model/contractsCollection';
import { CompletionItem, CompletionItemKind } from 'vscode-languageserver';
import { initialiseProject } from './projectService';
import * as vscode from 'vscode-languageserver';
import {Contract2, DeclarationType, DocumentContract, Function, SolidityCodeWalker, Variable, Struct} from './codeWalkerService';
import * as glob from 'glob';
import { relative } from 'path';
import { fileURLToPath } from 'url';
import * as path from 'path';


export class CompletionService {

    public rootPath: string;

    constructor(rootPath: string) {
        this.rootPath = rootPath;
    }

    public getTypeString(literal: any) {
        const isArray = literal.array_parts.length > 0;
        let isMapping = false;
        const literalType = literal.literal;
        let suffixType = '';

        if (typeof literalType.type !== 'undefined')  {
             isMapping = literalType.type === 'MappingExpression';
             if (isMapping) {
                suffixType = '(' + this.getTypeString(literalType.from) + ' => ' + this.getTypeString(literalType.to) + ')';
            }
        }

        if (isArray) {
            suffixType = suffixType + '[]';
        }

        if (isMapping) {
            return 'mapping' + suffixType;
        }

        return literalType + suffixType;
    }

    public createFunctionParamsSnippet(params: any, skipFirst: boolean = false): string {
        let paramsSnippet = '';
        let counter = 0;
        if (typeof params !== 'undefined' && params !== null) {
            params.forEach( parameterElement => {
               if(skipFirst && counter === 0) {
                  skipFirst = false; 
               } else {
                const typeString = this.getTypeString(parameterElement.literal);
                counter = counter + 1;
                const currentParamSnippet = '${' + counter + ':' + parameterElement.id + '}';
                    if (paramsSnippet === '') {
                        paramsSnippet = currentParamSnippet;
                    } else {
                        paramsSnippet = paramsSnippet + ', ' + currentParamSnippet;
                    }
                }
            });
        }
        return paramsSnippet;
    }

    public createParamsInfo(params: any): string {
        let paramsInfo = '';
        if (typeof params !== 'undefined' && params !== null) {
            if (params.hasOwnProperty('params')) {
                params = params.params;
            }
            params.forEach( parameterElement => {
               const typeString = this.getTypeString(parameterElement.literal);
                let currentParamInfo = '';
                if (typeof parameterElement.id !== 'undefined' && parameterElement.id !== null ) { // no name on return parameters
                    currentParamInfo = typeString + ' ' + parameterElement.id;
                } else {
                    currentParamInfo = typeString;
                }
                if (paramsInfo === '') {
                    paramsInfo = currentParamInfo;
                } else {
                    paramsInfo = paramsInfo + ', ' + currentParamInfo;
                }
            });
        }
        return paramsInfo;
    }

    public createFunctionEventCompletionItem(contractElement: any, type: string, contractName: string, skipFirstParamSnipppet: boolean = false): CompletionItem {

        const completionItem =  CompletionItem.create(contractElement.name);
        completionItem.kind = CompletionItemKind.Function;
        const paramsInfo = this.createParamsInfo(contractElement.params);
        const paramsSnippet = this.createFunctionParamsSnippet(contractElement.params, skipFirstParamSnipppet);
        let returnParamsInfo = this.createParamsInfo(contractElement.returnParams);
        if (returnParamsInfo !== '') {
            returnParamsInfo = ' returns (' + returnParamsInfo + ')';
        }
        completionItem.insertTextFormat = 2;
        completionItem.insertText = contractElement.name + '(' + paramsSnippet + ');';
        const info = '(' + type + ' in ' + contractName + ') ' + contractElement.name + '(' + paramsInfo + ')' + returnParamsInfo;
        completionItem.documentation = info;
        completionItem.detail = info;
        return completionItem;
    }

    public createParameterCompletionItem(contractElement: any, type: string, contractName: string): CompletionItem {

        const completionItem =  CompletionItem.create(contractElement.id);
        completionItem.kind = CompletionItemKind.Variable;
        const typeString = this.getTypeString(contractElement.literal);
        completionItem.detail = '(' + type + ' in ' + contractName + ') '
                                            + typeString + ' ' + contractElement.id;
        return completionItem;
    }

    public createVariableCompletionItem(contractElement: any, type: string, contractName: string): CompletionItem {

        const completionItem =  CompletionItem.create(contractElement.name);
        completionItem.kind = CompletionItemKind.Field;
        const typeString = this.getTypeString(contractElement.literal);
        completionItem.detail = '(' + type + ' in ' + contractName + ') '
                                            + typeString + ' ' + contractElement.name;
        return completionItem;
    }

    public createStructCompletionItem(contractElement: any, contractName: string): CompletionItem {

        const completionItem =  CompletionItem.create(contractElement.name);
        completionItem.kind = CompletionItemKind.Struct;
        //completionItem.insertText = contractName + '.' + contractElement.name;
        completionItem.insertText = contractElement.name;
        completionItem.detail = '(Struct in ' + contractName + ') '
                                            + contractElement.name;
        return completionItem;
    }

    public createEnumCompletionItem(contractElement: any, contractName: string): CompletionItem {

        const completionItem =  CompletionItem.create(contractElement.name);
        completionItem.kind = CompletionItemKind.Enum;
        //completionItem.insertText = contractName + '.' + contractElement.name;
        completionItem.insertText = contractElement.name;
        completionItem.detail = '(Enum in ' + contractName + ') '
                                            + contractElement.name;
        return completionItem;
    }
    
    // type "Contract, Libray, Abstract contract"
    public createContractCompletionItem(contractName: string, type: string): CompletionItem {

        const completionItem =  CompletionItem.create(contractName);
        completionItem.kind = CompletionItemKind.Class;
        completionItem.insertText = contractName;
        completionItem.detail = '(' + type + ' : ' + contractName + ') '
                        
        return completionItem;
    }

    public createInterfaceCompletionItem(contractName: string): CompletionItem {

        const completionItem =  CompletionItem.create(contractName);
        completionItem.kind = CompletionItemKind.Interface;
        completionItem.insertText = contractName;
        completionItem.detail = '( Interface : ' + contractName + ') '               
        return completionItem;
    }
  

    public getDocumentCompletionItems(documentText: string): CompletionItem[] {
        const completionItems = [];
        try {
            const result = solparse.parse(documentText);
            // console.log(JSON.stringify(result));
            // TODO struct, modifier
            result.body.forEach(element => {
                if (element.type === 'ContractStatement' ||  element.type === 'LibraryStatement' || element.type == 'InterfaceStatement') {
                    const contractName = element.name;
                    if (typeof element.body !== 'undefined' && element.body !== null) {
                        element.body.forEach(contractElement => {
                            if (contractElement.type === 'FunctionDeclaration') {
                                // ignore the constructor TODO add to contract initialiasation
                                if (contractElement.name !== contractName) {
                                    completionItems.push(
                                            this.createFunctionEventCompletionItem(contractElement, 'function', contractName ));
                                }
                            }

                            if (contractElement.type === 'EventDeclaration') {
                                completionItems.push(this.createFunctionEventCompletionItem(contractElement, 'event', contractName ));
                            }

                            if (contractElement.type === 'StateVariableDeclaration') {
                                completionItems.push(this.createVariableCompletionItem(contractElement, 'state variable', contractName));
                            }

                            if (contractElement.type === 'EnumDeclaration') {
                                completionItems.push(this.createEnumCompletionItem(contractElement, contractName));
                            }

                            if (contractElement.type === 'StructDeclaration') {
                                completionItems.push(this.createStructCompletionItem(contractElement, contractName));
                            }
                        });
                    }
                }
            });
        } catch (error) {
          // gracefule catch
          // console.log(error.message);
        }
        // console.log('file completion items' + completionItems.length);
        return completionItems;
    }


    public getAllCompletionItems2(packageDefaultDependenciesDirectory: string,
        packageDefaultDependenciesContractsDirectory: string,
        document: vscode.TextDocument,
        position: vscode.Position,
      ): CompletionItem[] {
        let completionItems = [];
        let triggeredByEmit = false;
        let triggeredByImport = false;
        let triggeredByDotStart = 0;
        try {
        var walker = new SolidityCodeWalker(this.rootPath,  packageDefaultDependenciesDirectory,
            packageDefaultDependenciesContractsDirectory,
        );
        const offset = document.offsetAt(position);

        var documentContractSelected = walker.getAllContracts(document, position);

        const lines = document.getText().split(/\r?\n/g);
        triggeredByDotStart = this.getTriggeredByDotStart(lines, position);
        
        //triggered by emit is only possible with ctrl space
        triggeredByEmit = getAutocompleteVariableNameTrimmingSpaces(lines[position.line], position.character - 1) === 'emit';
        triggeredByImport = getAutocompleteVariableNameTrimmingSpaces(lines[position.line], position.character - 1) === 'import';

        
        if(triggeredByDotStart > 0) {
            
            const globalVariableContext = GetContextualAutoCompleteByGlobalVariable(lines[position.line], triggeredByDotStart);
            
            if (globalVariableContext != null) {
                completionItems = completionItems.concat(globalVariableContext);
            } else {
                let autocompleteByDot = getAutocompleteTriggerByDotVariableName(lines[position.line], triggeredByDotStart - 1);
                // if triggered by variable //done
                // todo triggered by method (get return type) // done
                // todo triggered by property // done
                // todo variable // method return is an array (push, length etc)
                // variable / method / property is an address or other specific type functionality (balance, etc)
                // variable / method / property type is extended by a library

                if(autocompleteByDot.name !== '') {

                    
                    // have we got a selected contract (assuming not type.something)
                    if(documentContractSelected.selectedContract !== undefined && documentContractSelected.selectedContract !== null )
                    {
                        let selectedContract = documentContractSelected.selectedContract;

                        //this contract
                        if(autocompleteByDot.name === 'this' && autocompleteByDot.isVariable && autocompleteByDot.parentAutocomplete === null) {

                            //add selectd contract completion items
                            this.addContractCompletionItems(selectedContract, completionItems);

                        } else  {
                            /// the types 
                            let topParent = autocompleteByDot.getTopParent();
                            if (topParent.name === "this") {
                                topParent = topParent.childAutocomplete;
                            }

                            this.findDotCompletionItemsForSelectedContract(topParent, completionItems, documentContractSelected, documentContractSelected.selectedContract, offset);
                        }
                    }
                }
            }
            return completionItems;
        }

        if(triggeredByImport) {
            let files = glob.sync(this.rootPath + '/**/*.tosol');
            files.forEach(item => {
                let dependenciesDir = path.join(this.rootPath, packageDefaultDependenciesDirectory);
                item = path.join(item);
                if(item.startsWith(dependenciesDir)) {
                    let pathLibrary = item.substr(dependenciesDir.length + 1);
                    pathLibrary = pathLibrary.split('\\').join('/');
                    let completionItem = CompletionItem.create(pathLibrary);
                    completionItem.kind = CompletionItemKind.Reference;
                    completionItem.insertText = '"' + pathLibrary + '";';
                    completionItems.push(completionItem);
                } else {
                    let rel = relative(fileURLToPath(document.uri), item);
                    rel = rel.split('\\').join('/');
                    if(rel.startsWith('../'))
                    {
                        rel = rel.substr(1);
                    }
                    let completionItem = CompletionItem.create(rel);
                    completionItem.kind = CompletionItemKind.Reference;
                    completionItem.insertText = '"' + rel + '";';
                    completionItems.push(completionItem);
                }
            });
            return completionItems;
        }

        if(triggeredByEmit) {
           
            if(documentContractSelected.selectedContract !== undefined && documentContractSelected.selectedContract !== null ) {
                this.addAllEventsAsCompletionItems(documentContractSelected.selectedContract, completionItems);
            }

        } else {

            if(documentContractSelected.selectedContract !== undefined && documentContractSelected.selectedContract !== null ) {
                
                let selectedContract = documentContractSelected.selectedContract;
                this.addSelectedContractCompletionItems(selectedContract, completionItems, offset);
            }

            documentContractSelected.allContracts.forEach(x => {
            
                if(x.contractType === "ContractStatement") {
                completionItems.push(this.createContractCompletionItem(x.name, "Contract"));
            }
            
            if(x.contractType === "LibraryStatement") {
                    completionItems.push(this.createContractCompletionItem(x.name, "Library"));
                }

                if(x.contractType === "InterfaceStatement") {
                    completionItems.push(this.createInterfaceCompletionItem(x.name));
                }
            })
        }

    } catch (error) {
        // graceful catch
        console.log(error);
    } finally {

        completionItems = completionItems.concat(GetCompletionTypes());
        completionItems = completionItems.concat(GetCompletionKeywords());
        completionItems = completionItems.concat(GetCompletionUnits());
        completionItems = completionItems.concat(GetGlobalFunctions());
        completionItems = completionItems.concat(GetGlobalVariables());
        completionItems = completionItems.concat(GetFreeTonPragmas());
    }
    return completionItems;
    }



    private findDotCompletionItemsForSelectedContract(autocompleteByDot:AutocompleteByDot, completionItems: any[], documentContractSelected: DocumentContract, currentContract: Contract2, offset:number) {
        
        if(currentContract === documentContractSelected.selectedContract) {
            let selectedFunction = documentContractSelected.selectedContract.getSelectedFunction(offset);
            this.findDotCompletionItemsForContract(autocompleteByDot, completionItems, documentContractSelected.allContracts, documentContractSelected.selectedContract, selectedFunction, offset);
        } else {
            this.findDotCompletionItemsForContract(autocompleteByDot, completionItems, documentContractSelected.allContracts, documentContractSelected.selectedContract);
        }      
   
    }

    private findDotCompletionItemsForContract(autocompleteByDot:AutocompleteByDot, completionItems: any[], allContracts: Contract2[], currentContract: Contract2, selectedFunction: Function = null, offset:number = null) {
        
        let allStructs = currentContract.getAllStructs();
        let allEnums = currentContract.getAllEnums();
        let allVariables: Variable[] = currentContract.getAllStateVariables();
        let allfunctions: Function[] = currentContract.getAllFunctions();


        if(selectedFunction !== undefined && selectedFunction !== null)  {
            selectedFunction.findVariableDeclarationsInScope(offset, null);
            //adding input parameters
            allVariables = allVariables.concat(selectedFunction.input);
            //ading all variables
            allVariables = allVariables.concat(selectedFunction.variablesInScope);
        }
        
        
        let found = false;

        if (autocompleteByDot.isVariable) {
            
            allVariables.forEach(item => {
                if (item.name === autocompleteByDot.name && !found) {
                    found = true;
                    if(autocompleteByDot.childAutocomplete !== undefined && autocompleteByDot.childAutocomplete !== null) {
                        this.findDotType(allStructs, item.type, autocompleteByDot.childAutocomplete, completionItems, allContracts, currentContract);
                    } else {
                        this.findDotTypeCompletion(allStructs, item.type, completionItems, allContracts, currentContract);
                    }
                }
            });

            if (!found &&  (autocompleteByDot.childAutocomplete === undefined || autocompleteByDot.childAutocomplete === null)) {
                allEnums.forEach(item => {
                    if (item.name === autocompleteByDot.name) {
                        found = true;
                        item.items.forEach(property => {
                            let completitionItem = CompletionItem.create(property);
                            completionItems.push(completitionItem);
                        });
                    }
                });
            }

            if (!found && (autocompleteByDot.childAutocomplete === undefined || autocompleteByDot.childAutocomplete === null) ) {
                allContracts.forEach(item => {
                    if (item.name === autocompleteByDot.name) {
                        found = true;
                        this.addContractCompletionItems(item, completionItems);
                    }
                });
            }
        }

        if (autocompleteByDot.isMethod) {

            allfunctions.forEach(item => {
                if (item.name === autocompleteByDot.name) {
                    found = true;
                    if (item.output.length === 1) {
                        //todo return array
                        let type = item.output[0].type;

                        if(autocompleteByDot.childAutocomplete !== undefined && autocompleteByDot.childAutocomplete !== null) {
                            this.findDotType(allStructs, type, autocompleteByDot.childAutocomplete, completionItems, allContracts, currentContract);
                        } else {
                            this.findDotTypeCompletion(allStructs, type, completionItems, allContracts, currentContract);
                        }
                    }
                }
            });

            //contract declaration as IMyContract(address)
            if (!found && (autocompleteByDot.childAutocomplete === undefined || autocompleteByDot.childAutocomplete === null) ) {
                allContracts.forEach(item => {
                    if (item.name === autocompleteByDot.name) {
                        found = true;
                        this.addContractCompletionItems(item, completionItems);
                    }
                });
            }
        }
    }

  

    private findDotTypeCompletion(allStructs: Struct[], type: DeclarationType, completionItems: any[], allContracts: Contract2[], currentContract: Contract2) {
        let foundStruct = allStructs.find(x => x.name === type.name);
        if (foundStruct !== undefined) {
            foundStruct.variables.forEach(property => {
                //own method refactor
                let completitionItem = CompletionItem.create(property.name);
                const typeString = this.getTypeString(property.element.literal);
                completitionItem.detail = '(' + property.name + ' in ' + foundStruct.name + ') '
                    + typeString + ' ' + foundStruct.name;
                completionItems.push(completitionItem);
            });
        } else {

            let foundContract = allContracts.find(x => x.name === type.name);
            if (foundContract !== undefined) {
                foundContract.initialiseExtendContracts(allContracts);
                this.addContractCompletionItems(foundContract, completionItems);
            }
        }

        let allUsing = currentContract.getAllUsing(type);
        allUsing.forEach(usingItem => {
            let foundLibrary = allContracts.find(x => x.name === usingItem.name);
            if (foundLibrary !== undefined) {
                this.addAllLibraryExtensionsAsCompletionItems(foundLibrary, completionItems, type);
            }
        });
    }


    private findDotType(allStructs: Struct[], type: DeclarationType, autocompleteByDot: AutocompleteByDot, completionItems: any[], allContracts: Contract2[], currentContract: Contract2) {
        let foundStruct = allStructs.find(x => x.name === type.name);
        if (foundStruct !== undefined) {
            foundStruct.variables.forEach(property => {
                //own method refactor
                if(autocompleteByDot.name === property.name) {
                    if(autocompleteByDot.childAutocomplete !== undefined && autocompleteByDot.childAutocomplete !== null)  {
                        this.findDotType(allStructs, property.type, autocompleteByDot.childAutocomplete, completionItems, allContracts, currentContract);
                    } else {
                        this.findDotTypeCompletion(allStructs, property.type, completionItems, allContracts, currentContract);
                    }
                }
            });
        } else {

            let foundContract = allContracts.find(x => x.name === type.name);
            if (foundContract !== undefined) {
                foundContract.initialiseExtendContracts(allContracts);
                this.findDotCompletionItemsForContract(autocompleteByDot, completionItems, allContracts, foundContract);
                
            }
        }


        /*
        let allUsing = currentContract.getAllUsing(type);
        allUsing.forEach(usingItem => {
            let foundLibrary = allContracts.find(x => x.name === usingItem.name);
            if (foundLibrary !== undefined) {
                this.addAllLibraryExtensionsAsCompletionItems(foundLibrary, completionItems, type);
            }
        });
        */
    }


    private addContractCompletionItems(selectedContract: Contract2, completionItems: any[]) {
        this.addAllFunctionsAsCompletionItems(selectedContract, completionItems);

        this.addAllStateVariablesAsCompletionItems(selectedContract, completionItems);
    }

    private addSelectedContractCompletionItems(selectedContract: Contract2, completionItems: any[], offset: number) {
        this.addAllFunctionsAsCompletionItems(selectedContract, completionItems);

        this.addAllEventsAsCompletionItems(selectedContract, completionItems);

        this.addAllStateVariablesAsCompletionItems(selectedContract, completionItems);

        this.addAllStructsAsCompletionItems(selectedContract, completionItems);

        this.addAllEnumsAsCompletionItems(selectedContract, completionItems);

        let selectedFunction = selectedContract.getSelectedFunction(offset);

        if (selectedFunction !== undefined) {
            selectedFunction.findVariableDeclarationsInScope(offset, null);
            selectedFunction.input.forEach(parameter => {
                completionItems.push(this.createParameterCompletionItem(parameter.element, "function parameter", selectedFunction.contract.name));
            });
            selectedFunction.output.forEach(parameter => {
                completionItems.push(this.createParameterCompletionItem(parameter.element, "return parameter", selectedFunction.contract.name));
            });

            selectedFunction.variablesInScope.forEach(variable => {
                completionItems.push(this.createVariableCompletionItem(variable.element, "function variable", selectedFunction.contract.name));
            });
        }
    }

    private addAllEnumsAsCompletionItems(documentContractSelected: Contract2, completionItems: any[]) {
        let allEnums = documentContractSelected.getAllEnums();
        allEnums.forEach(item => {
            completionItems.push(
                this.createEnumCompletionItem(item.element, item.contract.name));
        });
    }

    private addAllStructsAsCompletionItems(documentContractSelected: Contract2, completionItems: any[]) {
        let allStructs = documentContractSelected.getAllStructs();
        allStructs.forEach(item => {
            completionItems.push(
                this.createStructCompletionItem(item.element, item.contract.name));
        });
    }

    private addAllEventsAsCompletionItems(documentContractSelected: Contract2, completionItems: any[]) {
        let allevents = documentContractSelected.getAllEvents();
        allevents.forEach(item => {
            completionItems.push(
                this.createFunctionEventCompletionItem(item.element, 'event', item.contract.name));
        });
    }

    private addAllStateVariablesAsCompletionItems(documentContractSelected: Contract2, completionItems: any[]) {
        let allStateVariables = documentContractSelected.getAllStateVariables();
        allStateVariables.forEach(item => {
            completionItems.push(
                this.createVariableCompletionItem(item.element, 'state variable', item.contract.name));
        });
    }

    private addAllFunctionsAsCompletionItems(documentContractSelected: Contract2, completionItems: any[]) {
        let allfunctions = documentContractSelected.getAllFunctions();
        allfunctions.forEach(item => {
            completionItems.push(
                this.createFunctionEventCompletionItem(item.element, 'function', item.contract.name));
        });
    }

    private addAllLibraryExtensionsAsCompletionItems(documentContractSelected: Contract2, completionItems: any[], type: DeclarationType) {
        let allfunctions = documentContractSelected.getAllFunctions();
        let filteredFunctions = allfunctions.filter( x => {
            if(x.input.length > 0 ) {
                let typex = x.input[0].type;
                let validTypeName = false;
                if(typex.name === type.name || (type.name === "address_payable" && typex.name === "address")) {
                    validTypeName = true;
                }
                 return typex.isArray === type.isArray && validTypeName && typex.isMapping === type.isMapping;
            }
            return false;
        });

        filteredFunctions.forEach(item => {
            completionItems.push(
                this.createFunctionEventCompletionItem(item.element, 'function', item.contract.name, true));
        });
    }

    public getTriggeredByDotStart(lines:string[], position: vscode.Position):number {
        let start = 0;
        let triggeredByDot = false;
        for (let i = position.character; i >= 0; i--) {
            if (lines[position.line[i]] === ' ') {
                triggeredByDot = false;
                i = 0;
                start = 0;
            }
            if (lines[position.line][i] === '.') {
                start = i;
                i = 0;
                triggeredByDot = true;
            }
        }
        return start;
    }

    public getAllCompletionItems(documentText: string,
                                documentPath: string,
                                packageDefaultDependenciesDirectory: string,
                                packageDefaultDependenciesContractsDirectory: string): CompletionItem[] {

        if (this.rootPath !== 'undefined' && this.rootPath !== null) {
            const contracts = new ContractCollection();
            contracts.addContractAndResolveImports(
                documentPath,
                documentText,
                initialiseProject(this.rootPath, packageDefaultDependenciesDirectory, packageDefaultDependenciesContractsDirectory));
            let completionItems = [];
            contracts.contracts.forEach(contract => {
                completionItems = completionItems.concat(this.getDocumentCompletionItems(contract.code));
            });
            // console.log('total completion items' + completionItems.length);
            return completionItems;
        } else {
            return this.getDocumentCompletionItems(documentText);
        }
    }
}

export function GetCompletionTypes(): CompletionItem[] {
    const completionItems = [];
    const types = ['address', 'string', 'bytes', 'byte', 'int', 'uint', 'bool', 'hash',
        `ExtraCurrencyCollection`, `TvmBuilder`, `TvmCell`, `TvmSlice`];
    for (let index = 8; index <= 256; index += 8) {
        types.push('int' + index);
        types.push('uint' + index);
        types.push('bytes' + index / 8);
        for(let m = 0; m <= 80; m++) {
            types.push(`fixed${index}x${m}`);
            types.push(`ufixed${index}x${m}`);
        }
    }
    types.forEach(type => {
        const completionItem =  CompletionItem.create(type);
        completionItem.kind = CompletionItemKind.Keyword;
        completionItem.detail = type + ' type';
        completionItems.push(completionItem);
    });
    // add mapping
    return completionItems;
}

function CreateCompletionItem(label: string, kind: CompletionItemKind, detail: string) {
    const completionItem = CompletionItem.create(label);
    completionItem.kind = kind;
    completionItem.detail = detail;
    return completionItem;
}

export function GetCompletionKeywords(): CompletionItem[] {
    const completionItems = [];
    const keywords = [ 'modifier', 'mapping', 'break', 'continue', 'delete', 'else', 'for',
    'if', 'new', 'return', 'returns', 'while', 'repeat', 'using',
    'private', 'public', 'external', 'internal', 'payable', 'nonpayable', 'view', 'pure', 'case', 'do', 'else', 'finally',
    'in', 'instanceof', 'return', 'throw', 'try', 'catch', 'typeof', 'yield', 'void', 'virtual', 'override',
    'responsible', 'functionID', 'inline', 'externalMsg', 'internalMsg'] ;
    keywords.forEach(unit => {
        const completionItem =  CompletionItem.create(unit);
        completionItem.kind = CompletionItemKind.Keyword;
        completionItems.push(completionItem);
    });

    completionItems.push(CreateCompletionItem('contract', CompletionItemKind.Class, null));
    completionItems.push(CreateCompletionItem('library', CompletionItemKind.Class, null));
    completionItems.push(CreateCompletionItem('storage', CompletionItemKind.Field, null));
    completionItems.push(CreateCompletionItem('memory', CompletionItemKind.Field, null));
    completionItems.push(CreateCompletionItem('var', CompletionItemKind.Field, null));
    completionItems.push(CreateCompletionItem('constant', CompletionItemKind.Constant, null));
    completionItems.push(CreateCompletionItem('immutable', CompletionItemKind.Keyword, null));
    completionItems.push(CreateCompletionItem('constructor', CompletionItemKind.Constructor, null));
    completionItems.push(CreateCompletionItem('event', CompletionItemKind.Event, null));
    completionItems.push(CreateCompletionItem('import', CompletionItemKind.Module, null));
    completionItems.push(CreateCompletionItem('enum', CompletionItemKind.Enum, null));
    completionItems.push(CreateCompletionItem('struct', CompletionItemKind.Struct, null));
    completionItems.push(CreateCompletionItem('function', CompletionItemKind.Function, null));

    return completionItems;
}


export function GetCompletionUnits(): CompletionItem[] {
    const completionItems = [];
    const etherUnits = ['wei', 'finney', 'szabo', 'ether',
        'nano', 'nanoton', 'ton', 'micro', 'microton', 'milli', 'milliton', 'kiloton', 'megaton', 'gigaton',
        'Ton', 'kTon', 'MTon', 'GTon', 'nTon'];
    etherUnits.forEach(unit => {
        const completionItem =  CompletionItem.create(unit);
        completionItem.kind = CompletionItemKind.Unit;
        completionItem.detail = unit + ': ether unit';
        completionItems.push(completionItem);
    });

    const timeUnits = ['seconds', 'minutes', 'hours', 'days', 'weeks', 'years'];
    timeUnits.forEach(unit => {
        const completionItem =  CompletionItem.create(unit);
        completionItem.kind = CompletionItemKind.Unit;

        if (unit !== 'years') {
            completionItem.detail = unit + ': time unit';
        } else {
            completionItem.detail = 'DEPRECATED: ' + unit + ': time unit';
        }
        completionItems.push(completionItem);
    });

    return completionItems;
}

export function GetFreeTonPragmas(): CompletionItem[] {
    return [
        {
            detail: 'pragma ton-solidity <version>',
            documentation: 'Used to reject compilation source file with some versions of the compiler',
            kind: CompletionItemKind.Text,
            insertText: 'pragma ton-solidity ${1:version};',
            insertTextFormat: 2,
            label: 'pragma ton-solidity',
        },
        {
            detail: 'pragma ignoreIntOverflow',
            documentation: 'Turns off binary operation result overflow check',
            kind: CompletionItemKind.Text,
            insertText: 'pragma ignoreIntOverflow;',
            insertTextFormat: 2,
            label: 'pragma ignoreIntOverflow',
        },
        {
            detail: 'pragma AbiHeader <header>',
            documentation: 'Force message forming utility to fill an appropriate field(s) in the header of the external inbound message to be sent to this contract:\nexpire - time at which message should be meant as expired;\npubkey - public key by which the message can be signed;\ntime - local time at what message was created',
            kind: CompletionItemKind.Text,
            insertText: 'pragma AbiHeader ${1:expire|pubkey|time};',
            insertTextFormat: 2,
            label: 'pragma AbiHeader',
        },
        {
            detail: 'pragma msgValue <value>',
            documentation: 'Allows specifying default value in nanotons attached to the internal messages that contract sends to call another contract. If it\'s not specified, this value is set to 10 000 000 nanotons',
            kind: CompletionItemKind.Text,
            insertText: 'pragma msgValue ${1:value};',
            insertTextFormat: 2,
            label: 'pragma msgValue',
        },
    ];
}

export function GetGlobalVariables(): CompletionItem[] {
    return [
        {
            detail: 'address',
            kind: CompletionItemKind.Variable,
            label: 'address',
        },
        {
            detail: 'Current block',
            kind: CompletionItemKind.Variable,
            label: 'block',
        },
        {
            detail: 'math namespace',
            documentation: 'TON math functions',
            kind: CompletionItemKind.Variable,
            label: 'math',
        },
        {
            detail: 'Current Message',
            kind: CompletionItemKind.Variable,
            label: 'msg',
        },
        {
            detail: '(uint) now',
			documentation: 'Current block timestamp (alias for block.timestamp)',
            kind: CompletionItemKind.Variable,
            label: 'now',
        },
        {
            detail: 'rnd namespace',
			documentation: 'The pseudorandom number generator',
            kind: CompletionItemKind.Variable,
            label: 'rnd',
        },
        {
            detail: 'tvm namespace',
            documentation: 'TON Virtual Machine instructions',
            kind: CompletionItemKind.Variable,
            label: 'tvm',
        },
        {
            detail: 'Current transaction',
            kind: CompletionItemKind.Variable,
            label: 'tx',
        },
        {
            detail: 'ABI encoding / decoding',
            kind: CompletionItemKind.Variable,
            label: 'abi',
        },
    ];
}

export function GetGlobalFunctions(): CompletionItem[] {
    return [
        {
            detail: 'assert(bool condition)',
			documentation: 'Throws if the condition is not met - to be used for internal errors.',
            insertText: 'assert(${1:condition});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Function,
            label: 'assert',
        },
        {
            detail: 'gasleft()',
			documentation: 'Returns the remaining gas',
            insertText: 'gasleft();',
            insertTextFormat: 2,
            kind: CompletionItemKind.Function,
            label: 'gasleft',
        },
        {
            detail: 'unicode: converts string into unicode',
            insertText: 'unicode"${1:text}"',
            insertTextFormat: 2,
            kind: CompletionItemKind.Function,
            label: 'unicode',
        },
        {
            detail: 'blockhash(uint blockNumber)',
			documentation: 'Hash of the given block - only works for 256 most recent, excluding current, blocks',
            insertText: 'blockhash(${1:blockNumber});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Function,
            label: 'blockhash',
        },
        {
            detail: 'gasToValue(uint128 gas, int8 wid) returns (uint128 value)',
			documentation: 'Returns worth of gas in workchain wid. Throws an exception if wid doesn\'t equal 0 and -1',
            insertText: 'gasToValue(${1:gas}, ${2:wid});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Function,
            label: 'gasToValue',
        },
        {
            detail: 'valueToGas(uint128 value, int8 wid) returns (uint128 gas)',
			documentation: 'Returns how much gas could be bought on value nanotons in workchain wid.\nThrows an exception if wid doesn\'t equal 0 and -1',
            insertText: 'valueToGas(${1:value}, ${2:wid});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Function,
            label: 'valueToGas',
        },
        {
            detail: 'format(string template, TypeA a, TypeB b, ...) returns (string)',
			documentation: 'Builds a string with arbitrary parameters. Empty placeholder {} can be filled with integer (in decimal view), address, string or fixed point number. Fixed point number is printed based on its type (fixedMxN is printed with N digits after dot).\nPlaceholder should be specified in such formats:\n"{}" - empty placeholder\n"{:[0]<width>{"x","d","X","t"}}" - placeholder for integers. Fills num with 0 if format starts with "0". Formats integer to have specified width. Can format integers in decimal ("d" postfix), lower hex ("x") or upper hex ("X") form. Format "t" prints number as a fixed point Ton sum (in nanotons).\nWarning: this function consumes too much gas, that\'s why it\'s better not to use it onchain',
            insertText: 'format(${1:template}, ${2:x});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'format',
        },
        {
            detail: 'logtvm(string, ...)',
			documentation: 'Dumps log string; logtvm is an alias for tvm.log(string)',
            insertText: 'logtvm(${1:log});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'logtvm',
        },
        {
            detail: 'stoi(string inputStr) returns (uint, bool)',
			documentation: 'Converts a string into an integer. String is meant to be number in decimal format, only if string starts with "0x" it will be converted from a hexadecimal format. Function returns the integer, that can be converted from uint to int and boolean status, which is false in case of illegal characters in the string.\nWarning: this function consumes too much gas, that\'s why it\'s better not to use it onchain',
            insertText: 'stoi(${1:inputStr});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'stoi',
        },
        {
            detail: 'require(bool condition)',
			documentation: 'Used to check the condition and throw an exception if the condition is not met. Throws with error code = 100',
            insertText: 'require(${1:condition});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'require',
        },
        {
            detail: 'require(bool condition, uint errorCode)',
			documentation: 'Used to check the condition and throw an exception if the condition is not met. The function takes condition and error code',
            insertText: 'require(${1:condition}, ${2:errorCode});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'require',
        },
        {
            detail: 'require(bool condition, uint errorCode, T exceptionArgument)',
			documentation: 'Used to check the condition and throw an exception if the condition is not met. The function takes condition, error code and the object of any type',
            insertText: 'require(${1:condition}, ${2:errorCode}, ${3:exceptionArgument});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'require',
        },
        {
            detail: 'revert()',
			documentation: 'Abort execution and revert state changes. Throws with error code = 100',
            insertText: 'revert();',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'revert',
        },
        {
            detail: 'revert(uint errorCode)',
			documentation: 'Abort execution and revert state changes. The function takes error code',
            insertText: 'revert(${1:errorCode});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'revert',
        },
        {
            detail: 'revert(uint errorCode, T exceptionArgument)',
			documentation: 'Abort execution and revert state changes. The function takes error code and the object of any type',
            insertText: 'revert(${1:errorCode}, ${2:exceptionArgument});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'revert',
        },
        {
            detail: 'bitSize(int x) returns (uint16)',
			documentation: 'Computes the smallest c ≥ 0 such that x fits into a c-bit signed integer (−2^(c−1) ≤ x < 2^(c−1))',
            insertText: 'bitSize(${1:x});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'bitSize',
        },
        {
            detail: 'uBitSize(int x) returns (uint16)',
			documentation: 'Computes the smallest c ≥ 0 such that x fits into a c-bit unsigned integer (0 ≤ x < 2^c)',
            insertText: 'uBitSize(${1:x});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'uBitSize',
        },
        {
            detail: 'selfdestruct(address dest_addr)',
			documentation: 'Creates and sends the message that carries all the remaining balance of the current smart contract and destroys the current account',
            insertText: 'selfdestruct(${1:dest_addr});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'selfdestruct',
        },
        {
            detail: 'addmod(uint x, uint y, uint k) returns (uint):' +
                    'compute (x + y) % k where the addition is performed with arbitrary precision and does not wrap around at 2**256',
            insertText: 'addmod(${1:x}, ${2:y}, ${3:k})',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'addmod',
        },
        {
            detail: 'mulmod(uint x, uint y, uint k) returns (uint):' +
                    'compute (x * y) % k where the multiplication is performed with arbitrary precision and does not wrap around at 2**256',
            insertText: 'mulmod(${1:x}, ${2:y}, ${3:k})',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'mulmod',
        },
        {
            detail: 'keccak256(...) returns (bytes32):' +
                    'compute the Ethereum-SHA-3 (Keccak-256) hash of the (tightly packed) arguments',
            insertText: 'keccak256(${1:x})',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'keccak256',
        },
        {
            detail: 'sha256(TvmSlice|bytes|string) returns (uint256): ' +
                    '1. Compute the SHA-256 hash. If the bit length of slice is not divisible by eight, throws a cell underflow exception. References of slice are not used to compute the hash. Only data bits located in the root cell of slice are used. ' +
                    '2. Compute the SHA-256 hash only for the first 127 bytes. If bytes.length > 127 then b[128], b[129], b[130] ... elements are ignored. ' +
                    '3. Same as for bytes: only the first 127 bytes are taken into account.',
            insertText: 'sha256(${1:x})',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'sha256',
        },
        {
            detail: 'sha256(...) returns (bytes32):' +
                    'compute the SHA-256 hash of the (tightly packed) arguments',
            insertText: 'sha256(${1:x})',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'sha256',
        },
        {
            detail: 'sha3(...) returns (bytes32):' +
                    'alias to keccak256',
            insertText: 'sha3(${1:x})',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'sha3',
        },
        {
            detail: 'ripemd160(...) returns (bytes20):' +
                    'compute RIPEMD-160 hash of the (tightly packed) arguments',
            insertText: 'ripemd160(${1:x})',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'ripemd160',
        },
        {
            detail: 'ecrecover(bytes32 hash, uint8 v, bytes32 r, bytes32 s) returns (address):' +
                    'recover the address associated with the public key from elliptic curve signature or return zero on error',
            insertText: 'ecrecover(${1:hash}, ${2:v}, ${3:r}, ${4:s})',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'ecrecover',
        },

    ];
}

export function GetContextualAutoCompleteByGlobalVariable(lineText: string, wordEndPosition: number): CompletionItem[] {
    if (isAutocompleteTrigeredByVariableName('address', lineText, wordEndPosition)) {
        return getAddressCompletionItems();
    }
    if (isAutocompleteTrigeredByVariableName('block', lineText, wordEndPosition)) {
        return getBlockCompletionItems();
    }
    if (isAutocompleteTrigeredByVariableName('math', lineText, wordEndPosition)) {
        return getMathCompletionItems();
    }
    if (isAutocompleteTrigeredByVariableName('msg', lineText, wordEndPosition)) {
        return getMsgCompletionItems();
    }
    if (isAutocompleteTrigeredByVariableName('rnd', lineText, wordEndPosition)) {
        return getRndCompletionItems();
    }
    if (isAutocompleteTrigeredByVariableName('tvm', lineText, wordEndPosition)) {
        return getTvmCompletionItems();
    }
    if (isAutocompleteTrigeredByVariableName('tx', lineText, wordEndPosition)) {
        return getTxCompletionItems();
    }
    if (isAutocompleteTrigeredByVariableName('abi', lineText, wordEndPosition)) {
        return getAbiCompletionItems();
    }
    return null;
}

function isAutocompleteTrigeredByVariableName(variableName: string, lineText: string, wordEndPosition: number): Boolean {
    const nameLength = variableName.length;
    if (wordEndPosition >= nameLength
        // does it equal our name?
        && lineText.substr(wordEndPosition - nameLength, nameLength) === variableName) {
          return true;
        }
    return false;
}

export class AutocompleteByDot {
    public isVariable: boolean = false;
    public isMethod: boolean = false;
    public isArray: boolean = false;
    public isProperty: boolean = false;
    public parentAutocomplete: AutocompleteByDot = null;// could be a property or a method
    public childAutocomplete: AutocompleteByDot = null;
    public name: string = '';

    getTopParent(): AutocompleteByDot {
        if(this.parentAutocomplete != null) {
            return this.parentAutocomplete.getTopParent();
        }
        return this;
    }
}


function getAutocompleteTriggerByDotVariableName(lineText: string, wordEndPosition:number): AutocompleteByDot {
    let searching = true;
    let result: AutocompleteByDot = new AutocompleteByDot();
    //simpler way might be to find the first space or beginning of line
    //and from there split / match (but for now kiss or slowly)

    wordEndPosition = getArrayStart(lineText, wordEndPosition, result);

    if(lineText[wordEndPosition] == ')' ) {
        result.isMethod = true;
        let methodParamBeginFound = false;
        while(!methodParamBeginFound && wordEndPosition >= 0 ) {
            if(lineText[wordEndPosition] === '(') {
                methodParamBeginFound = true;
            }
            wordEndPosition = wordEndPosition - 1;
        }
    }

    if(!result.isMethod && !result.isArray) {
        result.isVariable = true;
    }

    while(searching && wordEndPosition >= 0) {
        let currentChar = lineText[wordEndPosition];
        if(isAlphaNumeric(currentChar) || currentChar === '_' || currentChar === '$') {
            result.name = currentChar + result.name;
            wordEndPosition = wordEndPosition - 1;
        } else {
            if(currentChar === ' ') { // we only want a full word for a variable / method // this cannot be parsed due incomplete statements
                searching = false;
                return result;
            } else {
                if(currentChar === '.') {
                    result.parentAutocomplete = getAutocompleteTriggerByDotVariableName(lineText, wordEndPosition - 1);
                    result.parentAutocomplete.childAutocomplete = result;
                }
            }
            searching = false;
            return result;
        }
    }
    return result;
}


function getArrayStart(lineText: string, wordEndPosition: number, result: AutocompleteByDot) {
    if (lineText[wordEndPosition] == ']') {
        result.isArray = true;
        let arrayBeginFound = false;
        while (!arrayBeginFound && wordEndPosition >= 0) {
            if (lineText[wordEndPosition] === '[') {
                arrayBeginFound = true;
            }
            wordEndPosition = wordEndPosition - 1;
        }
    }
    if(lineText[wordEndPosition] == ']') {
        wordEndPosition = getArrayStart(lineText, wordEndPosition, result);
    }
    return wordEndPosition;
}

function getAutocompleteVariableNameTrimmingSpaces(lineText: string, wordEndPosition:number): string {
    let searching = true;
    let result: string = '';
    if(lineText[wordEndPosition] === ' ' ) {
        let spaceFound = true;
        while(spaceFound && wordEndPosition >= 0 ) {
            wordEndPosition = wordEndPosition - 1;
            if(lineText[wordEndPosition] !== ' ') {
                spaceFound = false;
            }
        }
    }

    while(searching && wordEndPosition >= 0) {
        let currentChar = lineText[wordEndPosition];
        if(isAlphaNumeric(currentChar) || currentChar === '_' || currentChar === '$') {
            result = currentChar + result;
            wordEndPosition = wordEndPosition - 1;
        } else {
            if(currentChar === ' ') { // we only want a full word for a variable // this cannot be parsed due incomplete statements
                searching = false;
                return result;
            }
            searching = false;
            return '';
        }
    }
    return result;
}

function isAlphaNumeric(str) {
    var code, i, len;
  
    for (i = 0, len = str.length; i < len; i++) {
      code = str.charCodeAt(i);
      if (!(code > 47 && code < 58) && // numeric (0-9)
          !(code > 64 && code < 91) && // upper alpha (A-Z)
          !(code > 96 && code < 123)) { // lower alpha (a-z)
        return false;
      }
    }
    return true;
  };

  function getAddressCompletionItems(): CompletionItem[] {
    return [
        {
            detail: 'makeAddrNone()',
			documentation: 'Constructs an address of type addr_none',
            kind: CompletionItemKind.Method,
            insertText: 'makeAddrNone()',
            insertTextFormat: 2,
            label: 'makeAddrNone',
        },
        {
            detail: 'makeAddrExtern(uint addrNumber, uint bitCnt)',
			documentation: 'Constructs an address of type addr_extern with given value with bitCnt bit length',
            kind: CompletionItemKind.Method,
            insertText: 'makeAddrExtern(${1:addrNumber}, ${2:bitCnt})',
            insertTextFormat: 2,
            label: 'makeAddrExtern',
        },
        {
            detail: 'makeAddrStd(int8 wid, uint address)',
			documentation: 'Constructs an address of type addr_std with given workchain id wid and value address_value',
            kind: CompletionItemKind.Method,
            insertText: 'makeAddrStd(${1:wid}, ${2:address})',
            insertTextFormat: 2,
            label: 'makeAddrStd',
        },
    ];
}

function getBlockCompletionItems(): CompletionItem[] {
    return [
        {
            detail: '(address)',
			documentation: 'Current block miner’s address',
            kind: CompletionItemKind.Property,
            label: 'coinbase',
        },
        {
            detail: '(bytes32)',
			documentation: 'DEPRICATED In 0.4.22 use blockhash(uint) instead. Hash of the given block - only works for 256 most recent blocks excluding current',
            insertText: 'blockhash(${1:blockNumber});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'blockhash',
        },
        {
            detail: '(uint)',
			documentation: 'Current block difficulty',
            kind: CompletionItemKind.Property,
            label: 'difficulty',
        },
        {
            detail: '(uint)',
			documentation: 'Current block gaslimit',
            kind: CompletionItemKind.Property,
            label: 'gaslimit',
        },
        {
            detail: '(uint)',
			documentation: 'Current block number',
            kind: CompletionItemKind.Property,
            label: 'number',
        },
        {
            detail: '(uint64)',
			documentation: 'The starting logical time of the current block',
            kind: CompletionItemKind.Property,
            label: 'timestamp',
        },
    ];
}

function getTxCompletionItems(): CompletionItem[] {
    return [
        {
            detail: '(uint)',
			documentation: 'Gas price of the transaction',
            kind: CompletionItemKind.Property,
            label: 'gas',
        },
        {
            detail: '(address)',
			documentation: 'Sender of the transaction (full call chain)',
            kind: CompletionItemKind.Property,
            label: 'origin',
        },
        {
            detail: '(uint64)',
			documentation: 'The logical time of the current transaction',
            kind: CompletionItemKind.Property,
            label: 'timestamp',
        },
    ];
}

function getMathCompletionItems(): CompletionItem[] {
    return [
        {
            detail: 'min(T a, T b, ...) returns (T)',
			documentation: 'Returns the minimal value of the passed arguments. T should be an integer or fixed point type',
            kind: CompletionItemKind.Method,
            insertText: 'min(${1:a}, ${2:b});',
            insertTextFormat: 2,
            label: 'min',
        },
        {
            detail: 'max(T a, T b, ...) returns (T)',
			documentation: 'Returns the maximal value of the passed arguments. T should be an integer or fixed point type',
            kind: CompletionItemKind.Method,
            insertText: 'max(${1:a}, ${2:b});',
            insertTextFormat: 2,
            label: 'max',
        },
        {
            detail: 'minmax(T a, T b) returns (T, T)',
			documentation: 'Returns minimal and maximal values of the passed arguments. T should be an integer or fixed point type',
            kind: CompletionItemKind.Method,
            insertText: 'minmax(${1:a}, ${2:b});',
            insertTextFormat: 2,
            label: 'minmax',
        },
        {
            detail: 'abs(int|fixed val) returns (int|fixed)',
			documentation: 'Computes the absolute value of the given integer.',
            kind: CompletionItemKind.Method,
            insertText: 'abs(${1:val});',
            insertTextFormat: 2,
            label: 'abs',
        },
        {
            detail: 'modpow2(uint value, uint power) returns (uint)',
			documentation: 'Computes the value modulo 2^power. Note that power should be a constant integer.',
            kind: CompletionItemKind.Method,
            insertText: 'modpow2(${1:value}, ${2:power});',
            insertTextFormat: 2,
            label: 'modpow2',
        },
        {
            detail: 'divc(T a, T b) returns (T)',
			documentation: 'Returns result of the division of two integers. The return value is rounded. ceiling mode is used. T should be an integer or fixed point type',
            kind: CompletionItemKind.Method,
            insertText: 'divc(${1:a}, ${2:b});',
            insertTextFormat: 2,
            label: 'divc',
        },
        {
            detail: 'divr(T a, T b) returns (T)',
			documentation: 'Returns result of the division of two integers. The return value is rounded. nearest mode is used. T should be an integer or fixed point type.',
            kind: CompletionItemKind.Method,
            insertText: 'divr(${1:a}, ${2:b});',
            insertTextFormat: 2,
            label: 'divr',
        },
        {
            detail: 'muldiv(T a, T b, T c) returns (T)',
			documentation: 'Multiplies two values and then divides the result by a third value. The return value is rounded. floor mode is used. T is integer type.',
            kind: CompletionItemKind.Method,
            insertText: 'muldiv(${1:a}, ${2:b}, ${2:c});',
            insertTextFormat: 2,
            label: 'muldiv',
        },
        {
            detail: 'muldivc(T a, T b, T c) returns (T)',
			documentation: 'Multiplies two values and then divides the result by a third value. The return value is rounded. ceiling mode is used. T is integer type.',
            kind: CompletionItemKind.Method,
            insertText: 'muldivc(${1:a}, ${2:b}, ${2:c});',
            insertTextFormat: 2,
            label: 'muldivc',
        },
        {
            detail: 'muldivr(T a, T b, T c) returns (T)',
			documentation: 'Multiplies two values and then divides the result by a third value. The return value is rounded. nearest mode is used. T is integer type.',
            kind: CompletionItemKind.Method,
            insertText: 'muldivr(${1:a}, ${2:b}, ${2:c});',
            insertTextFormat: 2,
            label: 'muldivr',
        },
        {
            detail: 'muldivmod(T a, T b, T c) returns (T, T)',
			documentation: 'Multiplies first two arguments, divides the result by third argument and returns the result and the remainder. Intermediate result is stored in the 514 bit buffer, and the final result is rounded to the floor.',
            kind: CompletionItemKind.Method,
            insertText: 'muldivmod(${1:a}, ${2:b}, ${2:c});',
            insertTextFormat: 2,
            label: 'muldivmod',
        },
        {
            detail: 'divmod(T a, T b) returns (T, T)',
			documentation: 'Divides the first number by the second one and returns the result and the remainder. Result is rounded to the floor. T is integer type.',
            kind: CompletionItemKind.Method,
            insertText: 'divmod(${1:a}, ${2:b});',
            insertTextFormat: 2,
            label: 'divmod',
        },
        {
            detail: 'sign(int val) returns (int8)',
			documentation: 'Returns -1 if val is negative; 0 if val is zero; 1 if val is positive.',
            kind: CompletionItemKind.Method,
            insertText: 'sign(${1:val});',
            insertTextFormat: 2,
            label: 'sign',
        },
    ]
}

function getMsgCompletionItems(): CompletionItem[] {
    return [
        {
            detail: '(uint32)',
			documentation: 'Returns a field "created_at" of the external inbound message',
            kind: CompletionItemKind.Property,
            label: 'createdAt',
        },
        {
            detail: '(ExtraCurrencyCollection)',
			documentation: 'Collections of arbitrary currencies contained in the balance of the inbound message',
            kind: CompletionItemKind.Property,
            label: 'currencies',
        },
        {
            detail: '(TvmSlice)',
			documentation: 'A payload of the inbound message',
            kind: CompletionItemKind.Property,
            label: 'data',
        },
        {
            detail: '(uint)',
			documentation: 'Remaining gas DEPRICATED in 0.4.21 use gasleft()',
            kind: CompletionItemKind.Property,
            label: 'gas',
        },
        {
            detail: '(bool)',
			documentation: 'Is the contract is called by external message',
            kind: CompletionItemKind.Property,
            label: 'isExternal',
        },
        {
            detail: '(bool)',
			documentation: 'Is the contract is called by internal message',
            kind: CompletionItemKind.Property,
            label: 'isInternal',
        },
        {
            detail: '(bool)',
			documentation: 'Is the contract is called by tick/tock transactions',
            kind: CompletionItemKind.Property,
            label: 'isTickTock',
        },
        {
            detail: 'pubkey() returns (uint256)',
			documentation: 'Returns sender\'s public key, obtained from the body of the external inbound message. If the message is not signed, msg.pubkey() returns 0. If the message is signed and message header (pragma AbiHeader) does not contain pubkey then msg.pubkey() is equal to tvm.pubkey()',
            kind: CompletionItemKind.Method,
            insertText: 'pubkey();',
            insertTextFormat: 2,
            label: 'pubkey',
        },
        {
            detail: '(address)',
			documentation: 'Sender of the message for internal message; address(0) for external message; address(0) for tick/tock transaction.',
            kind: CompletionItemKind.Property,
            label: 'sender',
        },
        {
            detail: '(bytes4)',
			documentation: 'First four bytes of the calldata (i.e. function identifier)',
            kind: CompletionItemKind.Property,
            label: 'sig',
        },
        {
            detail: '(uint128)',
			documentation: 'Balance of the inbound message in nanotons for internal message; 0 for external message; Undefined value for tick/tock transaction.',
            kind: CompletionItemKind.Property,
            label: 'value',
        },
    ];
}

function getRndCompletionItems(): CompletionItem[] {
    return [
        {
            detail: 'next() returns (uint256)',
			documentation: 'Generates a new pseudo-random number',
            kind: CompletionItemKind.Method,
            insertText: 'next();',
            insertTextFormat: 2,
            label: 'next',
        },
        {
            detail: 'next(T limit) returns (T):',
			documentation: 'Generates a new pseudo-random number.\n If the first argument limit > 0 then function returns the value in the range 0..limit-1. Else if limit < 0 then the returned value lies in range limit..-1. Else if limit == 0 then it returns 0',
            kind: CompletionItemKind.Method,
            insertText: 'next(${1:limit});',
            insertTextFormat: 2,
            label: 'next',
        },
        {
            detail: 'getSeed() returns (uint256)',
			documentation: 'Returns the current random seed',
            kind: CompletionItemKind.Method,
            insertText: 'getSeed();',
            insertTextFormat: 2,
            label: 'getSeed',
        },
        {
            detail: 'setSeed(uint256 x)',
			documentation: 'Sets the random seed to x',
            kind: CompletionItemKind.Method,
            insertText: 'setSeed(${1:x});',
            insertTextFormat: 2,
            label: 'setSeed',
        },
        {
            detail: 'shuffle()',
			documentation: 'Randomizes the random seed. Mixes the random seed and the logical time of the current transaction. The result is set as the random seed',
            kind: CompletionItemKind.Method,
            insertText: 'shuffle();',
            insertTextFormat: 2,
            label: 'shuffle',
        },
        {
            detail: 'shuffle(uint someNumber)',
			documentation: 'Randomizes the random seed. Mixes the random seed and someNumber. The result is set as the random seed',
            kind: CompletionItemKind.Method,
            insertText: 'shuffle(${1:someNumber});',
            insertTextFormat: 2,
            label: 'shuffle',
        }
    ]
}

function getTvmCompletionItems(): CompletionItem[] {
    return [
        {
            detail: 'accept()',
            documentation: 'Executes TVM instruction "ACCEPT" (TVM - A.11.2. - F800). This instruction sets current gas limit to its maximal allowed value. This action is required to process external messages, which bring no value.',
            kind: CompletionItemKind.Method,
            insertText: 'accept();',
            insertTextFormat: 2,
            label: 'accept',
        },
        {
            detail: 'commit()',
			documentation: 'Creates a "check point" of the state variables (by copying them from c7 to c4) and register c5. If the contract throws an exception at the computing phase then the state variables and register c5 will roll back to the "check point", and the computing phase will be considered "successful". If contract doesn\'t throw an exception, it has no effect.',
            kind: CompletionItemKind.Method,
            insertText: 'commit();',
            insertTextFormat: 2,
            label: 'commit',
        },
        {
            detail: 'rawCommit()',
			documentation: 'Same as tvm.commit() but doesn\'t copy the state variables from c7 to c4. It\'s a wrapper for opcode COMMIT. See TVM.\n Note: Don\'t use tvm.rawCommit() after tvm.accept() in processing external messages because you don\'t save from c7 to c4 the hidden state variable timestamp, which is used for replay protection.',
            kind: CompletionItemKind.Method,
            insertText: 'rawCommit();',
            insertTextFormat: 2,
            label: 'rawCommit',
        },
        {
            detail: 'getData() returns (TvmCell)',
			documentation: 'An experimental function. A dual of the tvm.setData() returning value of c4 register. Getting a raw storage cell is useful when upgrading a new version of contract that introduces an altered data layout. Manipulation with a raw storage cell requires an understanding of the way the compiler layouts the data. Refer to the description of tvm.setData() to get more details. Note: state variables and replay protection timestamp stored in data cell have the same values that were before the transaction. See tvm.commit() how to update register c4.',
            kind: CompletionItemKind.Method,
            insertText: 'getData();',
            insertTextFormat: 2,
            label: 'getData',
        },
        {
            detail: 'setData(TvmCell data)',
			documentation: 'An experimental function. Set cell data to register c4. Note, after returning from a public function all state variable from c7 will copy to c4 and tvm.setData will have no effect. Be careful with the hidden state variable timestamp and think about possibility of replaying external messages.',
            kind: CompletionItemKind.Method,
            insertText: 'setData(${1:data});',
            insertTextFormat: 2,
            label: 'setData',
        },
        {
            detail: 'log(log, ...)',
			documentation: 'Dumps log string. This function is wrapper for TVM instructions PRINTSTR (for constant literal strings shorter than 16 symbols) and STRDUMP (for other strings). logtvm is an alias for tvm.log(string). Note: For long strings dumps only the first 127 symbols.',
            kind: CompletionItemKind.Method,
            insertText: 'log(${1:log});',
            insertTextFormat: 2,
            label: 'log',
        },
        {
            detail: 'hexdump(T a)',
			documentation: 'Dumps cell data or integer in hex format. Note that for cells this function dumps data only from the first cell. T must be an integer type or TvmCell',
            kind: CompletionItemKind.Method,
            insertText: 'hexdump(${1:a});',
            insertTextFormat: 2,
            label: 'hexdump',
        },
        {
            detail: 'bindump(T a)',
			documentation: 'Dumps cell data or integer in bin format. Note that for cells this function dumps data only from the first cell. T must be an integer type or TvmCell',
            kind: CompletionItemKind.Method,
            insertText: 'bindump(${1:a});',
            insertTextFormat: 2,
            label: 'bindump',
        },
        {
            detail: 'setcode(TvmCell newCode)',
			documentation: 'Creates an output action that would change this smart contract code to that given by Cell newCode (this change will take effect only after the successful termination of the current run of the smart contract)',
            kind: CompletionItemKind.Method,
            insertText: 'setcode(${1:newCode})',
            insertTextFormat: 2,
            label: 'setcode',
        },
        {
            detail: 'configParam(uint8 paramNumber) returns (TypeA a, TypeB b, ...)',
			documentation: 'Executes TVM instruction "CONFIGPARAM" (TVM - A.11.4. - F832). This command returns the value of the global configuration parameter with integer index paramNumber. Argument should be an integer literal. Supported paramNumbers: 1, 15, 17, 34',
            kind: CompletionItemKind.Method,
            insertText: 'configParam(${1:paramNumber});',
            insertTextFormat: 2,
            label: 'configParam',
        },
        {
            detail: 'rawConfigParam(uint8 paramNumber) returns (TvmCell cell, bool status)',
			documentation: 'Executes TVM instruction "CONFIGPARAM" (TVM - A.11.4. - F832). This command returns the value of the global configuration parameter with integer index paramNumber as a cell and a boolean status',
            kind: CompletionItemKind.Method,
            insertText: 'rawConfigParam(${1:paramNumber});',
            insertTextFormat: 2,
            label: 'rawConfigParam',
        },
        {
            detail: 'rawReserve(uint value, uint8 flag)',
			documentation: 'Creates an output action which reserves reserve nanotons. It is roughly equivalent to create an outbound message carrying reserve nanotons to oneself, so that the subsequent output actions would not be able to spend more money than the remainder. It\'s a wrapper for opcode "RAWRESERVE"',
            kind: CompletionItemKind.Method,
            insertText: 'rawReserve(${1:value}, ${2:flag});',
            insertTextFormat: 2,
            label: 'rawReserve',
        },
        {
            detail: 'rawReserve(uint value, ExtraCurrencyCollection currency, uint8 flag)',
			documentation: 'Creates an output action which reserves reserve nanotons. It is roughly equivalent to create an outbound message carrying reserve nanotons to oneself, so that the subsequent output actions would not be able to spend more money than the remainder. It\'s a wrapper for opcode "RAWRESERVEX"',
            kind: CompletionItemKind.Method,
            insertText: 'rawReserve(${1:value}, ${2:currency}, ${3:flag});',
            insertTextFormat: 2,
            label: 'rawReserve',
        },
        {
            detail: 'hash(TvmCell|string|bytes|TvmSlice data) returns (uint256)',
			documentation: 'Executes TVM instruction "HASHCU" or "HASHSU" (TVM - A.11.6. - F900). It computes the representation hash of a given argument and returns it as a 256-bit unsigned integer. For string and bytes it computes hash of the tree of cells, which contains data, but not data itself. Use sha256 to count hash of data',
            kind: CompletionItemKind.Method,
            insertText: 'hash(${1:data});',
            insertTextFormat: 2,
            label: 'hash',
        },
        {
            detail: 'checkSign(uint256 hash, uint256 SignHighPart, uint256 SignLowPart, uint256 pubkey) returns (bool)',
			documentation: 'Executes TVM instruction "CHKSIGNU" (TVM - A.11.6. - F910). This command checks the Ed25519-signature of a hash using public key pubkey. Signature is represented by two uint256 SignHighPart and SignLowPart',
            kind: CompletionItemKind.Method,
            insertText: 'checkSign(${1:hash}, ${2:SignHighPart}, ${3:SignLowPart}, ${4:pubkey});',
            insertTextFormat: 2,
            label: 'checkSign',
        },
        {
            detail: 'checkSign(uint256 hash, TvmSlice signature, uint256 pubkey) returns (bool)',
			documentation: 'Executes TVM instruction "CHKSIGNU" (TVM - A.11.6. - F910). This command checks the Ed25519-signature of a hash using public key pubkey. Signature is represented by a slice signature',
            kind: CompletionItemKind.Method,
            insertText: 'checkSign(${1:hash}, ${2:signature}, ${3:pubkey});',
            insertTextFormat: 2,
            label: 'checkSign',
        },
        {
            detail: 'checkSign(TvmSlice data, TvmSlice signature, uint256 pubkey) returns (bool)',
			documentation: 'Executes TVM instruction "CHKSIGNS" (TVM - A.11.6. - F911). This command checks Ed25519-signature of data using public key pubkey. Signature is represented by a slice signature',
            kind: CompletionItemKind.Method,
            insertText: 'checkSign(${1:data}, ${2:signature}, ${3:pubkey});',
            insertTextFormat: 2,
            label: 'checkSign',
        },
        {
            detail: 'insertPubkey(TvmCell stateInit, uint256 pubkey) returns (TvmCell)',
			documentation: 'Inserts a public key into stateInit data field. If stateInit has wrong format then throws exception',
            kind: CompletionItemKind.Method,
            insertText: 'insertPubkey(${1:stateInit}, ${2:pubkey});',
            insertTextFormat: 2,
            label: 'insertPubkey',
        },
        {
            detail: 'buildStateInit(TvmCell code, TvmCell data) returns (TvmCell stateInit)',
			documentation: 'Generates a StateInit (TBLKCH - 3.1.7.) from code and data',
            kind: CompletionItemKind.Method,
            insertText: 'buildStateInit(${1:code}, ${2:data});',
            insertTextFormat: 2,
            label: 'buildStateInit',
        },
        {
            detail: 'buildStateInit(TvmCell code, TvmCell data, uint8 splitDepth) returns (TvmCell stateInit)',
			documentation: 'Generates a StateInit (TBLKCH - 3.1.7.) from code and data',
            kind: CompletionItemKind.Method,
            insertText: 'buildStateInit(${1:code}, ${2:data}, ${2:splitDepth});',
            insertTextFormat: 2,
            label: 'buildStateInit',
        },
        {
            detail: 'buildStateInit(struct stateInit) returns (TvmCell stateInit)',
			documentation: 'Generates a StateInit (TBLKCH - 3.1.7.) from code and data. The struct is {code: TvmCell code, data: TvmCell data, splitDepth: uint8 splitDepth, pubkey: uint256 pubkey, contr: contract Contract, varInit: {VarName0: varValue0, ...}}',
            kind: CompletionItemKind.Method,
            insertText: 'buildStateInit(${1:stateInit});',
            insertTextFormat: 2,
            label: 'buildStateInit',
        },
        {
            detail: 'buildEmptyData(uint256 publicKey) returns (TvmCell)',
			documentation: 'Generates a persistent storage of the contract that contains only public key',
            kind: CompletionItemKind.Method,
            insertText: 'buildEmptyData(${1:publicKey});',
            insertTextFormat: 2,
            label: 'buildEmptyData',
        },
        {
            detail: 'deploy(TvmCell stateInit, TvmCell payload, uint128 value, int8 wid) returns(address)',
			documentation: 'Deploys a new contract and returns the address of the deployed contract. This function may be useful if you want to write a universal contract that can deploy any contract. In another cases, use Deploy via new',
            kind: CompletionItemKind.Method,
            insertText: 'deploy(${1:stateInit}, ${2:payload}, ${3:value}, ${4:wid});',
            insertTextFormat: 2,
            label: 'deploy',
        },
        {
            detail: 'code() returns (TvmCell)',
			documentation: 'Returns contract\'s code',
            kind: CompletionItemKind.Method,
            insertText: 'code();',
            insertTextFormat: 2,
            label: 'code',
        },
        {
            detail: 'codeSalt(TvmCell code) returns (optional(TvmCell) optSalt)',
			documentation: 'If code contains salt then optSalt contains one. Otherwise, optSalt doesn\'t contain any value',
            kind: CompletionItemKind.Method,
            insertText: 'codeSalt(${1:code});',
            insertTextFormat: 2,
            label: 'codeSalt',
        },
        {
            detail: 'setCodeSalt(TvmCell code, TvmCell salt) returns (TvmCell newCode)',
			documentation: 'Inserts salt into code and returns new code newCode',
            kind: CompletionItemKind.Method,
            insertText: 'setCodeSalt(${1:code}, ${2:salt});',
            insertTextFormat: 2,
            label: 'setCodeSalt',
        },
        {
            detail: 'pubkey() returns (uint256)',
			documentation: 'Returns contract\'s public key, stored in contract data. If key is not set, function returns 0',
            kind: CompletionItemKind.Method,
            insertText: 'pubkey();',
            insertTextFormat: 2,
            label: 'pubkey',
        },
        {
            detail: 'setPubkey(uint256 newPubkey)',
			documentation: 'Set new contract\'s public key. Contract\'s public key can be obtained from tvm.pubkey',
            kind: CompletionItemKind.Method,
            insertText: 'setPubkey(${1:newPubkey});',
            insertTextFormat: 2,
            label: 'setPubkey',
        },
        {
            detail: 'setCurrentCode(TvmCell newCode)',
			documentation: 'Changes this smart contract current code to that given by Cell newCode. Unlike tvm.setcode() this function changes code of the smart contract only for current TVM execution, but has no effect after termination of the current run of the smart contract',
            kind: CompletionItemKind.Method,
            insertText: 'setCurrentCode(${1:newCode});',
            insertTextFormat: 2,
            label: 'setCurrentCode',
        },
        {
            detail: 'resetStorage()',
			documentation: 'Resets all state variables to their default values',
            kind: CompletionItemKind.Method,
            insertText: 'resetStorage();',
            insertTextFormat: 2,
            label: 'resetStorage',
        },
        {
            detail: 'functionId(functionName|ContractName) returns (uint32)',
			documentation: 'Returns a function id for public/external function or constructor',
            kind: CompletionItemKind.Method,
            insertText: 'functionId(${1:name});',
            insertTextFormat: 2,
            label: 'functionId',
        },
        {
            detail: 'encodeBody(function, arg0, arg1, ...) returns (TvmCell)',
			documentation: 'Constructs a function call message body that can be used as the payload for <address>.transfer()',
            kind: CompletionItemKind.Method,
            insertText: 'encodeBody(${1:function}, ${2:arg0});',
            insertTextFormat: 2,
            label: 'encodeBody',
        },
        {
            detail: 'encodeBody(function, callbackFunction, arg0, arg1, ...) returns (TvmCell)',
			documentation: 'Constructs a responsible function call message body that can be used as the payload for <address>.transfer()',
            kind: CompletionItemKind.Method,
            insertText: 'encodeBody(${1:function}, ${2:callbackFunction}, ${3:arg0});',
            insertTextFormat: 2,
            label: 'encodeBody',
        },
        {
            detail: 'exit()',
			documentation: 'Used to save state variables and to quickly terminate execution of the smart contract. Exit code is equal to zero',
            kind: CompletionItemKind.Method,
            insertText: 'exit();',
            insertTextFormat: 2,
            label: 'exit',
        },
        {
            detail: 'exit1()',
			documentation: 'Used to save state variables and to quickly terminate execution of the smart contract. Exit code is equal to one',
            kind: CompletionItemKind.Method,
            insertText: 'exit1();',
            insertTextFormat: 2,
            label: 'exit1',
        },
        {
            detail: 'buildExtMsg(struct extMsg) returns (TvmCell)',
			documentation: 'Should be used only offchain and intended to be used only in debot contracts. Allows creating an external inbound message, that calls the func function of the contract on address destination with specified function arguments. Mandatory parameters that are used to form a src address field that is used for debots: abiVer - ABI version; callbackId - identifier of the callback function; onErrorId - identifier of the function that is called in case of error; signBoxHandle - handle of the sign box entity, that engine will use to sign the message',
            kind: CompletionItemKind.Method,
            insertText: 'buildExtMsg(${1:extMsg});',
            insertTextFormat: 2,
            label: 'buildExtMsg',
        },
        {
            detail: 'buildIntMsg({dest: address, value: uint128, call: {function, [callbackFunction,] arg0, arg1, arg2, ...}, bounce: bool, currencies: ExtraCurrencyCollection}) returns (TvmCell)',
			documentation: 'Generates an internal outbound message that contains function call. The cell can be used to send a message using tvm.sendrawmsg(). If the function is responsible then callbackFunction parameter must be set. dest, value and call parameters are mandatory. Another parameters can be omitted. See <address>.transfer() where these options and their default values are described',
            kind: CompletionItemKind.Method,
            insertText: 'buildIntMsg(${intMsg});',
            insertTextFormat: 2,
            label: 'buildIntMsg',
        },
        {
            detail: 'sendrawmsg(TvmCell msg, uint8 flag)',
            documentation: 'Send the internal/external message msg with flag. It\'s wrapper for opcode SENDRAWMSG (TVM - A.11.10). Internal message msg can be generated by tvm.buildIntMsg(). Possible values of flag are the same as in <address>.transfer()',
            kind: CompletionItemKind.Method,
            insertText: 'sendrawmsg(${1:msg}, ${2:flag});',
            insertTextFormat: 2,
            label: 'sendrawmsg',
        },
    ];
}

function getAbiCompletionItems(): CompletionItem[] {
    return [
        {
            detail: 'encode(..) returns (bytes)',
			documentation: 'ABI-encodes the given arguments',
            insertText: 'encode(${1:arg});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'encode',
        },
        {
            detail: 'encodePacked(..) returns (bytes)',
			documentation: 'Performes packed encoding of the given arguments',
            insertText: 'encodePacked(${1:arg});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'encodePacked',
        },
        {
            detail: 'encodeWithSelector(bytes4,...) returns (bytes)',
			documentation: 'ABI-encodes the given arguments starting from the second and prepends the given four-byte selector',
            insertText: 'encodeWithSelector(${1:bytes4}, ${2:arg});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'encodeWithSelector',
        },
        {
            detail: 'encodeWithSignature(string,...) returns (bytes)',
			documentation: 'Equivalent to abi.encodeWithSelector(bytes4(keccak256(signature), ...)',
            insertText: 'encodeWithSignature(${1:signatureString}, ${2:arg});',
            insertTextFormat: 2,
            kind: CompletionItemKind.Method,
            label: 'encodeWithSignature',
        },
    ];
}
