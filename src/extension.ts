'use strict';
import * as path from 'path';
import * as vscode from 'vscode';
// import { Compiler } from './compiler';
import { LanguageClient, LanguageClientOptions, ServerOptions, TransportKind, RevealOutputChannelOn, WorkspaceChange } from 'vscode-languageclient';
// tslint:disable-next-line:no-duplicate-imports
import { workspace, WorkspaceFolder } from 'vscode';
import { formatDocument } from './formatter/prettierFormatter';
// import { compilerType } from './solcCompiler';

let diagnosticCollection: vscode.DiagnosticCollection;
// let compiler: Compiler;

export async function activate(context: vscode.ExtensionContext) {
    const ws = workspace.workspaceFolders;
    diagnosticCollection = vscode.languages.createDiagnosticCollection('tosolidity');
    // compiler = new Compiler(context.extensionPath);

    context.subscriptions.push(diagnosticCollection);

    // context.subscriptions.push(vscode.commands.registerCommand('tosolidity.compilerInfo', async () => {
    //     await compiler.outputCompilerInfoEnsuringInitialised();
    // }));

    // context.subscriptions.push(vscode.commands.registerCommand('tosolidity.solcReleases', async () => {
    //     compiler.outputSolcReleases();
    // }));

    // context.subscriptions.push(vscode.commands.registerCommand('tosolidity.selectWorkspaceRemoteSolcVersion', async () => {
    //     compiler.selectRemoteVersion(vscode.ConfigurationTarget.Workspace);
    // }));

    // context.subscriptions.push(vscode.commands.registerCommand('tosolidity.downloadRemoteSolcVersion', async () => {
    //     const root = vscode.workspace.workspaceFolders[0];
    //     compiler.downloadRemoteVersion(root.uri.fsPath);
    // }));

    // context.subscriptions.push(vscode.commands.registerCommand('tosolidity.downloadRemoteVersionAndSetLocalPathSetting', async () => {
    //     const root = vscode.workspace.workspaceFolders[0];
    //     compiler.downloadRemoteVersionAndSetLocalPathSetting(vscode.ConfigurationTarget.Workspace, root.uri.fsPath);
    // }));


    // context.subscriptions.push(vscode.commands.registerCommand('tosolidity.selectGlobalRemoteSolcVersion', async () => {
    //     compiler.selectRemoteVersion(vscode.ConfigurationTarget.Global);
    // }));

    // context.subscriptions.push(vscode.commands.registerCommand('tosolidity.changeDefaultCompilerType', async () => {
    //     compiler.changeDefaultCompilerType(vscode.ConfigurationTarget.Workspace);
    // }));


    context.subscriptions.push(
        vscode.languages.registerDocumentFormattingEditProvider('tosolidity', {
            provideDocumentFormattingEdits(document: vscode.TextDocument): vscode.TextEdit[] {
                return formatDocument(document, context);
            },
        }));

    const serverModule = path.join(__dirname, 'server.js');
    const serverOptions: ServerOptions = {
        debug: {
            module: serverModule,
            options: {
                execArgv: ['--nolazy', '--inspect=6009'],
            },
            transport: TransportKind.ipc,
        },
        run: {
            module: serverModule,
            transport: TransportKind.ipc,
        },
    };

    const clientOptions: LanguageClientOptions = {
        documentSelector: [
            { language: 'tosolidity', scheme: 'file' },
            { language: 'tosolidity', scheme: 'untitled' },
        ],
        revealOutputChannelOn: RevealOutputChannelOn.Never,
        synchronize: {
            // Synchronize the setting section 'tosolidity' to the server
            configurationSection: 'tosolidity',
            // Notify the server about file changes to '.tosol.js files contain in the workspace (TODO node, linter)
            // fileEvents: vscode.workspace.createFileSystemWatcher('**/.tosol.js'),
        },
        initializationOptions: context.extensionPath,
    };

    let clientDisposable;

    // if (ws) {
        clientDisposable = new LanguageClient(
            'tosolidity',
            'Free TON Solidity Language Server',
            serverOptions,
            clientOptions).start();
    // }
    // Push the disposable to the context's subscriptions so that the
    // client can be deactivated on extension deactivation
    context.subscriptions.push(clientDisposable);
}
